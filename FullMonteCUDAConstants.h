#ifndef _FULLMONTECUDACONSTANTS_H_
#define _FULLMONTECUDACONSTANTS_H_

// the minimum compute capability for FullMonte CUDA
// TODO: this is not actually the minimum, its just
// the compute capability I stared with. It would be good
// to find out the minimum (we require double precision)
// and update it here.
#define MIN_COMPUTE_CAPABILITY_MAJOR 6
#define MIN_COMPUTE_CAPABILITY_MINOR 1

// the speed of light in mm/ns
#define SPEED_OF_LIGHT_MM_PER_NS 299.792458f

// max number of materials
#define MAX_CUDA_MATERIALS 32

#endif 

