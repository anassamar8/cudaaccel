#pragma once

/**
 * This file contains various (helper) __device__ functions used by the main
 * 'full_monte_device.cu' file. This file is intended to be included
 * once (in that full_monte_device.cu) and only once. Moreover, this
 * file is not intended to be included in the host (don't put shareable
 * constants here) and should be included in the build (the nvcc build).
 * In general THERE SHOULD BE NO __global__ FUNCTIONS IN HERE - they won't
 * get built.
 */

#include "FullMonteCUDATypes.h"
#include "FullMonteCUDAConstants.h"

#include "helper_cuda.h"
#include "helper_math.h"
#include "math_constants.h"

#include "tinymt/tinymt32_status.cuh"
#include "tinymt/tinymt32_kernel.cuh"

#include <stdio.h>
#include <cfloat>

using namespace CUDA;

/**
 * Calculates if a given photon with position p and direction d
 * intersects with a face of the current tetra.
 *
 * @param T the tetra
 * @param pkt the packet
 * @returns the StepResult
 */
__device__ StepResult getIntersection(
    const Tetra& T,
    const Packet& pkt
)
{
    StepResult result;
    result.idx = -1;

    //
    // compute n (dot) d for all faces of T
    //      dot[face] = n[face] (dot) packet direction
    //
    // also calculate constraint if point lies within the tetra.
    // Which also represents the height which the photon position
    // is over the face of the respective Tetra
    //      h1[face] = C - n[face] (dot) packet position
    //
    float dots[4];
    float h1[4];

    // TODO: rearrange normals of Tetra so they are already in this format?
    const float3 n[4] = {
        {T.nx.x, T.ny.x, T.nz.x},
        {T.nx.y, T.ny.y, T.nz.y},
        {T.nx.z, T.ny.z, T.nz.z},
        {T.nx.w, T.ny.w, T.nz.w}
    };
    const float C[4] = {T.C.x, T.C.y, T.C.z, T.C.w};

    #pragma unroll 4
    for(int f = 0; f < 4; f++) {
        dots[f] = dot(n[f], pkt.dir.d);
        h1[f] = C[f] - dot(n[f], pkt.p);
    }

    //
    // dist is the distance of the photon to the 4 faces of the tetra.
    // After distance is found, check if dot is positive or negative. If
    // it is negative use the value of dist, otherwise use infinity.
    //
    float dist[4];
    #pragma unroll 4
    for(int f = 0; f < 4; f++) {
        // NOTE: just less than? or equal too?
        if(dots[f] < 0) {
            dist[f] = h1[f] / dots[f];
        } else {
            dist[f] = FLT_MAX;
        }
    }

    //
    // at most 3 points of the dot products should be negative:
    // height  dot     h/dot   meaning
    //  +       +       +       OK: inside, facing away (no possible intersection)
    //  +       -       -       OK: inside, facing towards (intersection possible)
    //  -       +       -       OK: outside, facing in (this is the entry face with roundoff error, no possible intersection)
    //  -       -       +       ERROR: outside, facing out (problem!! this must be the entry face, but exiting!)
    //
    // require n dot p - C > 0 (above face) and d dot n < 0
    // Determines the minimum value of dist and also returns its index (0, 1, 2, 3)
    //
    uint min_idx = 0;
    float min_idx_val = dist[0];

    for(int f = 0; f < 4; f++) {
        if(dist[f] < min_idx_val) {
            min_idx_val = dist[f];
            min_idx = f;
        }
    }

    //
    // Is the smallest distance of the photon smaller than the random step length s?
    // If yes, return true. This means that the photon intersects with one of the tetras
    // faces within the random step length
    //
    // if min_idx_val[0] < s[0]
    //
    result.hit = min_idx_val < pkt.s.x;  // min_idx_val < pkt.s[0]

    //
    // Choose the face with the smallest distance to the photons position and store it in
    // IDfe. Bitwise AND of the index in min_idx because the index can also have the 
    // value 4 which would result in bad memory access (size of IDfds is 4 [0-3]
    //
    const unsigned int IDfds[4] = {T.IDfds.x, T.IDfds.y, T.IDfds.z, T.IDfds.w};
    result.IDfe = IDfds[min_idx&3];

    //
    // Choose the adjacent tetra of the nearest face to the photons position and store it in
    // IDte. Bitwise AND of the index in min_idx because the index can also have the 
    // value 4 which would result in bad memory access (size of IDte is 4 [0-3])
    //
    const unsigned int adjTetras[4] = {T.adjTetras.x, T.adjTetras.y, T.adjTetras.z, T.adjTetras.w};
    result.IDte = adjTetras[min_idx&3];

    // will be 4 if no min found
    result.idx = min_idx;

    // compare the smallest distance min_idx_val with the random step length
    // and store the smaller value in result.distance
    result.distance = fmin(min_idx_val, pkt.s.x);  // pkt.s[0]
    result.distance = fmax(result.distance, 0.0);

    //
    // Determine the new position of the photon after the step length.
    //
    result.Pe = pkt.p + (pkt.dir.d * result.distance);

    return result;
}

/**
 * Given a spin matrix [[costheta, sintheta], [cosphi, sinphi]) 'spins' the current packet.
 *
 * @param pkt the packet
 * @param costheta spin matrix [0][0]
 * @param sintheta spin matrix [0][1]
 * @param cosphi spin matrix [1][0]
 * @param sinphi spin matrix [1][1]
 */
__device__ void spin_packet(
    Packet&         pkt,
    const float     costheta,
    const float     sintheta,
    const float     cosphi,
    const float     sinphi
)
{
    // the original direction vectors
    const float3 d0 = pkt.dir.d;
    const float3 a0 = pkt.dir.a;
    const float3 b0 = pkt.dir.b;

    // 'spin' the direction vectors based on the (random) spin matrix
    // d, a and b all remain orthonormal.
    pkt.dir.d = costheta*d0 - sintheta*cosphi*a0 + sintheta*sinphi*b0;
    pkt.dir.a = sintheta*d0 + costheta*cosphi*a0 - costheta*sinphi*b0;
    pkt.dir.b = sinphi*a0 + cosphi*b0;
}


/**
 * Scatters a packet in the material
 *
 * @param pkt the packet to scatter
 * @param mat the current material
 * @param rng_state the random number generator for this thread
 */
__device__ void scatter(
    Packet&                 pkt,
    const Material&         mat,
    tinymt32_status_t*      rng_state
)
{
    // generate random variables for spin matrix
    const float phi = 2.0f * CUDART_PI_F * tinymt32_single(rng_state);  // U[0,2PI)
    const float q = 2.0f * tinymt32_single(rng_state) - 1.0f;  // U[-1,1)

    float t, costheta;
    if(mat.isotropic) {  // isotropic --> g ~= 0.0
        costheta = q;
    } else {
        // t = (1-g*g)/(1.0 + g*q)
        t = (mat.one_minus_gg) * __frcp_rn(1.0f + mat.g*q);

        // costheta = 1/(2*g) * (1 + g*g - t*t)
        costheta = clamp(mat.recip_2g * (mat.one_plus_gg - t*t), -1.0f, 1.0f);  // clamp this to [-1,1]
    }

    const float sintheta = sqrt(1.0f-(costheta*costheta));

    // sincos() should be faster than sin() and cos() on their own
    float sinphi, cosphi;
    sincosf(phi, &sinphi, &cosphi);

    // spin the packet
    spin_packet(
        pkt,
        costheta,
        sintheta,
        cosphi,
        sinphi
    );
}

/**
 * Peforms reflection and refraction when the intersection occurs between
 * tetras with materials of different refractive indices ('n'). The packet
 * can either reflect or have its angle to the normal refract. Packet is
 * moving from a material with n = ni, to a material with n = nt.
 *
 * @param pkt the packet
 * @param currTetra the current tetra (material has n = ni)
 * @param stepResult the current stepResult
 * @param ni refractive index of material packet is moving FROM
 * @param nt refractive index of material packet is moving TO
 * @param rng_state the random number generator for this thread
 * @param IDt_next the next tetra in the path (output of this function)
 */
__device__ void reflect_refract(
    Packet&                 pkt,
    const Tetra&            currTetra,
    const StepResult&       stepResult,
    const float             ni,
    const float             nt,
    tinymt32_status_t*      rng_state,
    unsigned int&           IDt_next
)
{
    // the face normals of the tetra
    const float3 Ns[4] = {
        {currTetra.nx.x, currTetra.ny.x, currTetra.nz.x},
        {currTetra.nx.y, currTetra.ny.y, currTetra.nz.y},
        {currTetra.nx.z, currTetra.ny.z, currTetra.nz.z},
        {currTetra.nx.w, currTetra.ny.w, currTetra.nz.w}
    };

    // the face normal at the closest face
    const float3 normal = Ns[stepResult.idx];

    const float ni_nt_ratio = ni / nt;
    const float cosi = fmin(1.0f, -dot(normal, pkt.dir.d));
    const float sini = sqrt(1.0f-(cosi*cosi));
    const float sint = ni_nt_ratio * sini;
    const float cost = sqrt(1.0f-(sint*sint));

    // the new direction set below
    float3 newd;

    // check for Total Internal Reflection (TIR)
    if(1.0f < sint) {
        // reflect:
        //  d' = d + 2*(d (dot) n) * n
        //     = d + 2*cosi*n
        newd = pkt.dir.d + 2.0f*cosi*normal;
    } else {
        const float3 d_p = pkt.dir.d + cosi * normal;

        const float root_Rs = (ni*cosi - nt*cost) / (ni*cosi + nt*cost);
        const float root_Rp = (ni*cost - nt*cosi) / (ni*cost + nt*cosi);
        const float pr = 0.5f * (root_Rs*root_Rs + root_Rp*root_Rp);

        const float rn = tinymt32_single(rng_state);

        // do fresnel reflection probabilisticly
        if(rn < pr) {
            // reflect
            newd = pkt.dir.d + 2.0f*cosi*normal;
        } else {
            newd = ni_nt_ratio*d_p - cost*normal;
            IDt_next = stepResult.IDte;
        }
    }

    // find the index of the smallest component of the new 'd' direction vector
    const float D[3] = {newd.x, newd.y, newd.z};
    unsigned m = 0, i, j;
    float min_D = fabs(D[0]);
    for(int t = 1; t < 3; t++) {
        if(fabs(D[t]) < min_D) {
            m = t;
            min_D = fabs(D[t]);
        }
    }
    i = (m+1)%3;
    j = (m+2)%3;
    
    // compute a vector normal to the new 'd' by zeroing the min element in d and taking
    // the transpose (i.e. d = {dx, dy, dz} -> a = {-dy, dx, 0} (assuming |dz| < |dy|, |dx|)
    const float k = 1.0f/sqrt(D[i]*D[i] + D[j]*D[j]);
    float aa[3];
    aa[m] = 0.0f;
    aa[i] = D[j] * k;
    aa[j] = -D[i] * k;

    // set the direction vectors
    pkt.dir.d = newd;
    pkt.dir.a = {aa[0], aa[1], aa[2]};
    pkt.dir.b = cross(pkt.dir.d, pkt.dir.a);
}

/**
 * enum for roulette outcomes
 */
typedef enum {
    RouletteWin,
    RouletteLose,
    TimeGate,
    Continue
} TerminationResult;

/**
 * Performs the termination check (roulette).
 *
 * @param pkt the packet to check for termination
 * @param rng_state the random number generator for this thread.
 * @param wmin the minimum weight before roulette happens
 * @param prwin the percentage of winning roulette
 * @returns the result of the termination
 */
__device__ TerminationResult terminationCheck(
    Packet&                 pkt,
    tinymt32_status_t*      rng_state,
    const float             wmin,
    const float             prwin
)
{
    TerminationResult term;
    
    // if the packet weight is less than the minimum (wmin)
    // the packet will enter roulette, otherwise it continues
    if(pkt.w < wmin) {
        // roulette
        const float rn = tinymt32_single(rng_state);

        if(rn < prwin) {
            // wins roulette, increase the energy of the packet by
            // the chance it had of losing
            pkt.w /= prwin;
            term = RouletteWin;
        } else {
            // packet lost roulette - time to die
            term = RouletteLose;
        }
    } else {
        // not time for roulette yet
        term = Continue;
    }

    return term;
}

