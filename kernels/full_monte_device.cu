#include "FullMonteCUDATypes.h"
#include "FullMonteCUDAConstants.h"

#include "full_monte_device.cuh"
#include "full_monte_internals.cu"

#include "tinymt/tinymt32_status.cuh"
#include "tinymt/tinymt32_kernel.cuh"

#include "math_constants.h"

#include <stdio.h>

using namespace CUDA;

/////////////////////////////////////////////
//// constants
__constant__ Material   materials_cache[MAX_CUDA_MATERIALS];
/////////////////////////////////////////////

/**
 * KERNEL
 * The FullMonteCUDA main kernel.
 *
 * @param seed the base RNG seed
 * @param num_packets the number of packets being simulated
 * @param Nstep_max the maximum number of steps a packet can take 
 * @param Nhit_max the maximum number of times an intersection event can happen per step
 * @param wmin the minimum weight of a packet before it will go through roulette
 * @param prwin the percentage chance that a packet wins roulette
 * @param launched_packets array of packets that have been launched already (from the host, 1 per thread)
 * @param tetras the tetras that make up the mesh
 * @param rng_state the random number generators (RNG) for the threads (1 per thread = 1 per packet)
 * @param scoreVolume whether to score volume absorption events
 * @param scoreDirectedSurface whether to score surface enter/exit events
 * @param scoreSurfaceExit whether to score mesh surface exit events
 *
 * @param volumeEnergy the absorption array for each tetra (1 entry per tetra) - CAN BE NULL
 * @param meshExitEnergy energy exiting the surface of the mesh - CAN BE NULL
 * @param faceEnergy1 energy going through face of tetra (WITH THE NORMAL) - CAN BE NULL
 * @param faceEnergy2 energy going through face of tetra (AGAINST THE NORMAL) - CAN BE NULL
 */
__global__ void full_monte_device(
    // Input
    const unsigned int                  seed, 
    const unsigned int                  num_packets,
    const unsigned int                  Nstep_max,
    const unsigned int                  Nhit_max,
    const float                         wmin,
    const float                         prwin,
    const LaunchPacket* __restrict__    launched_packets,
    const Tetra* __restrict__           tetras,
    tinymt32_status_t* __restrict__     rng_state,

    bool                                scoreVolume,
    bool                                scoreDirectedSurface,
    bool                                scoreSurfaceExit,

    // Output
    double* __restrict__                volumeEnergy,
    double* __restrict__                meshExitEnergy,
    double* __restrict__                faceEnergy1,
    double* __restrict__                faceEnergy2
)
{
    // use shared memory to store the rng_state variable for EACH thread
    extern __shared__ tinymt32_status_t rng_state_shared[];
    
    // the packet index
    const unsigned int pkt_idx = blockIdx.x * blockDim.x + threadIdx.x;

    // check if this packet actually needs to be run
    if(pkt_idx >= num_packets) {
        return;
    }

    // get the index of the packet within this block
    const unsigned int loc_pkt_idx = threadIdx.x;

    // each thread is responsible for loading its own rng_state into shared memory
    // NOTE: we do not need a __syncthreads() since each thread only uses its own rng_state
    rng_state_shared[loc_pkt_idx] = rng_state[pkt_idx];

    // initialize the RNG for this thread (packet).
    // seed + idx is so that each thread gets its own unique seed.
    tinymt32_init(&rng_state_shared[loc_pkt_idx], seed+pkt_idx);

    //// LAUNCH ////
    // grab the launched packet
    const LaunchPacket lpkt = launched_packets[pkt_idx];

    //// MAIN COMPUTATION ////
    unsigned Nhit, Nstep;
    StepResult stepResult;
    Tetra currTetra = tetras[lpkt.tetraID];
    Material currMat = materials_cache[currTetra.matID];

    unsigned IDt = lpkt.tetraID;
    unsigned IDt_next = IDt, IDm = currTetra.matID, IDm_next = IDm, IDm_bound;
    float ni, nt;

    // create the initial packet
    Packet pkt = {
       .dir = lpkt.dir,
       .p = lpkt.p,
       .s = {0., 0., 0.},
       .w = 1.0f
    };

    // acummulation buffer variables
    int prev_accum_ID = -1;
    double prev_accum_val = 0.0;

    // STEP/HOP LOOP: Outer loop to keep the packet stepping through the mesh
    for(Nstep = 0; Nstep < Nstep_max; ++Nstep) {
        //// STEP ////
        // draw a random step length
        // 2.0 - [1,2) = [1.0, 0) avoiding 0 for log(x)
        const float f_exp_rn = -log(2.0f - tinymt32_single12(&rng_state_shared[loc_pkt_idx]));
        pkt.s = f_exp_rn * currMat.m_init;

        // compute step result, this moves the packet and checks for a tetra intersection
        stepResult = getIntersection(currTetra, pkt);

        // TODO: assert(0 <= pkt.s[0]);
        // TODO: assert(pkt.s[0] < 1e3);  // source in air ?

        // update the packets position based on the step result
        pkt.p = stepResult.Pe;
        
        // INTERSECTION LOOP: Inner loop while a step crosses a tetrahedral boundary
        for(Nhit = 0; stepResult.hit && Nhit < Nhit_max; ++Nhit) {
            // bad step result check
            if(stepResult.idx > 3) {
                return;
            }

            // update step based on material properties.
            pkt.s = pkt.s + (stepResult.distance * currMat.m_prop);

            // check the next tetras material ID
            IDm_bound = tetras[stepResult.IDte].matID;

            // check if we are changing materials
            if(IDm == IDm_bound) {  // not changing materials
                IDt_next = stepResult.IDte;
            } else {  // changing materials
                nt = materials_cache[IDm_bound].n;
                ni = currMat.n;

                // check if the refractive index of the materials differs
                if(ni == nt) {
                    // move to the next tetra
                    IDt_next = stepResult.IDte;
                } else {
                    //// REFLECT/REFRACT ////
                    // this function (may) modify 'pkt' and 'IDt_next'
                    reflect_refract(
                        pkt,
                        currTetra,
                        stepResult,
                        ni,
                        nt,
                        &rng_state_shared[loc_pkt_idx],
                        IDt_next
                    );
                }
            }

            // check for tetra change
            if(IDt != IDt_next) {
                if(IDt_next == 0) {
                    //// EXIT MESH ////
                    // score surface exit if enabled
                    if(scoreSurfaceExit) {
                        // stepResult.IDfe is directed, shift by 1 to get undirected index of face 
                        atomicAdd(&meshExitEnergy[stepResult.IDfe >> 1], pkt.w);
                    }
                    return;
                } else {
                    //// NEW TETRA ////

                    // score directed surface event if enabled
                    // only do so if directed surface scoring is enabled AND if the face of the
                    // current tetra which the packet is passing is marked for fluence scoring.
                    // see the definition of CUDA::Tetra in FullMonteCUDATypes.h for faceFlags details
                    if(scoreDirectedSurface && (currTetra.faceFlags >> (stepResult.idx << 3) & 0x1)) {
                        // stepResult.IDfe is a directed face so lower bit represents the surface direction
                        // (0 = with normal, 1= against normal). By convention faceEnergy1 is wit normal
                        // and faceEnergy2 is against normal. So check the low bit of the directed face
                        // to determine which array to accumulate into
                        if(stepResult.IDfe & 0x1) {
                            // against normal
                            atomicAdd(&faceEnergy2[stepResult.IDfe >> 1], pkt.w);
                        } else {
                            // with normal
                            atomicAdd(&faceEnergy1[stepResult.IDfe >> 1], pkt.w);
                        }
                    }

                    // move to the next tetra
                    IDt = IDt_next;
                    IDm_next = IDm_bound;
                    currTetra = tetras[IDt];
                }
            }

            // check if we changed materials
            if(IDm != IDm_next) {
                IDm = IDm_next;
                currMat = materials_cache[IDm];

                pkt.s.x = pkt.s.y * currMat.recip_mu_t;  // pkt.s[0] = pkt.s[1] / currMat.mu_t
                pkt.s.z = 0.0f;  // pkt.s[2] = 0. Reset time.
            }

            // do the remaining step
            stepResult = getIntersection(currTetra, pkt);

            // update packet position
            pkt.p = stepResult.Pe;

        } // END: INTERSECTION LOOP

        // check if too many tetra intersections occured
        if(Nhit >= Nhit_max) {
            return;
        }
        
        //// ABSORB ////
        if(scoreVolume) {
            const double pktWeightLoss = pkt.w * currMat.absfrac;
            if(pktWeightLoss > 0.0) {
                // drop the weight in the packet
                pkt.w -= pktWeightLoss;
            
                // absorb the energy in the current tetra
                if(prev_accum_ID == -1) {
                    // special case first absorption - store it locally
                    prev_accum_ID = IDt;
                    prev_accum_val = pktWeightLoss;
                } else if(prev_accum_ID == IDt) {
                    // previous stored accumulation is same tetra as current absorb - do it locally
                    prev_accum_val += pktWeightLoss;
                } else {
                    // previous and current absorb are in different tetras - push back previous and store current
                    atomicAdd(&volumeEnergy[prev_accum_ID], prev_accum_val);
                    prev_accum_ID = IDt;
                    prev_accum_val = pktWeightLoss;
                }
            }
        }

        //// ROULETTE ////
        TerminationResult term_result = terminationCheck(pkt, &rng_state_shared[loc_pkt_idx], wmin, prwin);
        switch(term_result) {
            case Continue:
            case RouletteWin:
                break;
            case RouletteLose:
            case TimeGate:
                return;
        }

        //// SPIN ////
        scatter(pkt, currMat, &rng_state_shared[loc_pkt_idx]);

    }  // END: STEP/HOP LOOP
}


/////////////////////////////////////////////////////
// Below is the caller for the above kernel.
// Provides an abstraction between CUDA and C++ code.
/////////////////////////////////////////////////////

/**
 * Simply calls the full_monte_device kernel (see above).
 * This function is asynchronous.
 */
void full_monte_device_caller(
    // Input
    const unsigned int      seed,
    const unsigned int      blocks,
    const unsigned int      threads,
    const unsigned int      num_packets,
    const unsigned int      Nstep_max,
    const unsigned int      Nhit_max,
    const float             wmin,
    const float             prwin,
    const LaunchPacket*     launched_packets,
    const Tetra*            tetras,
    tinymt32_status_t*      rng_state,
    bool                    scoreVolume,
    bool                    scoreDirectedSurface,
    bool                    scoreSurfaceExit,

    // Output
    double*                 volumeEnergy,
    double*                 meshExitEnergy,
    double*                 faceEnergy1,
    double*                 faceEnergy2
)
{
    // launch the kernel
    // NOTE: currently we need 1 tinymt32_status_t object per thread in shared memory
    full_monte_device<<<blocks, threads, threads * sizeof(tinymt32_status_t)>>>(
        seed,
        num_packets,
        Nstep_max,
        Nhit_max,
        wmin,
        prwin,
        launched_packets,
        tetras,
        rng_state,
        scoreVolume,
        scoreDirectedSurface,
        scoreSurfaceExit,

        volumeEnergy,
        meshExitEnergy,
        faceEnergy1,
        faceEnergy2
    );

    // check errors in launch
    getLastCudaError("Error in launch of full_monte_device.");
}

/////////////////////////////////////////////////////////////
// Helpers
/////////////////////////////////////////////////////////////

/**
 * Copy materials to local cache. No linker in nvcc, must do this in this file.
 *
 * @param materials the list of materials to store in the 'materials_cache' constant memory
 */
void copyMaterialsToCache(const std::vector<Material>& materials) {
    checkCudaErrors(cudaMemcpyToSymbol(materials_cache, &materials[0], materials.size() * sizeof(Material), 0, cudaMemcpyHostToDevice));
}

