/**
 * \mainpage
 *
 * This is CUDA implementation of tiny Mersenne Twister pseudorandom number
 * generator(tinymt).
 *
 * \section sc1 Functions
 * The following files are intended to work with kernel programs written
 * by users.
 * - tinymt32_kernel.cuh tinymt32 device functions.
 *   - tinymt32_init()
 *   - tinymt32_uint32()
 *   - tinymt32_single()
 *   - tinymt32_single12()
 * - tinymt64_kernel.cuh tinymt64 device functions.
 *   - tinymt64_init()
 *   - tinymt64_uint64()
 *   - tinymt64_double()
 *   - tinymt64_double12()
 *
 * The following files are intended to work with host programs written
 * by users.
 * - tinymt32_host.h a header file of reading tinymt32 parameters from file
 * - tinymt32_host.c a file of reading tinymt32 parameters from file
 * - tinymt64_host.h a header file of reading tinymt64 parameters from file
 * - tinymt64_host.c a file of reading tinymt64 parameters from file
 *
 * \section sc2 Samples
 * The following files are sample kernel programs which call the functions in
 * tinymt32_kernel.cuh or tinymt64_kernel.cuh
 * - sample32_kernel.cu
 * - sample64_kernel.cu
 *
 * The following files are sample host programs which call the functions in
 * sample32_kernel.cu or sample64_kernel.cu
 * - sample32.cu
 * - sample64.cu
 *
 * Four executable files are made by typing \b make \b all.
 * - sample32 a sample program for tinymt32
 * - sample32-test a sample program which measures elapsed time
 * - sample64 a sample program for tinymt64
 * - sample64-test a sample program which measures elapsed time
 *
 * \section sc3 Tools
 * The following file contains utility functions for host program
 * to call cuda functions.
 * - test_tool.hpp
 *
 * \section sc4 Authors
 * @author Mutsuo Saito, (saito@math.sci.hiroshima-u.ac.jp) Hiroshima University
 * @author Makoto Matsumoto, The University of Tokyo
 *
 * @date 2011-9-16
 *
 * \section sc5 Copyright and License
 * Copyright (C) 2011 Mutsuo Saito, Makoto Matsumoto, Hiroshima
 * University and The University of Tokyo. All rights reserved.
 *
 * The 3-clause BSD License is applied to this software.
 * \verbinclude LICENSE.txt
 */
