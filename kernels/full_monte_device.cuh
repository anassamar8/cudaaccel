#pragma once

#include "../FullMonteCUDATypes.h"

#include "tinymt/tinymt32_status.cuh"

//
// NOTE: do NOT include 'tinymt/tinymt32_kernel.cuh' here so
// we can include this file in the host.
//

#include <vector>

/////////////////////////////////////////////
void full_monte_device_caller(
    // Input
    const unsigned int          seed,
    const unsigned int          blocks,
    const unsigned int          threads,
    const unsigned int          num_packets,
    const unsigned int          Nstep_max,
    const unsigned int          Nhit_max,
    const float                 wmin,
    const float                 prwin,
    const CUDA::LaunchPacket*   launched_packets,
    const CUDA::Tetra*          tetras,
    tinymt32_status_t*          rng_state,
    bool                        scoreVolume,
    bool                        scoreDirectedSurface,
    bool                        scoreSurfaceExit,

    // Output
    double*                     volumeEnergy,
    double*                     meshExitEnergy,
    double*                     faceEnergy1,
    double*                     faceEnergy2
);

void copyMaterialsToCache(const std::vector<CUDA::Material>& materials);
/////////////////////////////////////////////

