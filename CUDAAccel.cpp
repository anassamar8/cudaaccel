#include "CUDAAccel.h"
#include "FullMonteCUDATypes.h"
#include "FullMonteCUDAConstants.h"

#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <ctime>

// NOTE: ORDER IMPORTANT
#include <cuda_runtime.h>
#include "helper_cuda.h"

// safety multlipliers for memory allocation
// whenever we give memory estimates we use this
// percentage of the max for safety
#define MEMORY_SAFETY_FACTOR 0.8

using namespace CUDA;

/** Constructor */
CUDAAccel::CUDAAccel() {
    devId = 0;
    
    maxThreadsPerBlock = 0;
    prefThreadsPerBlock = 0;
    maxBlocks = 0;

    seed = 7;
    NstepMax = 10000;
    NhitMax = 1000;
    facesCount = 0;
    wMin = 1e-4;
    prWin = 0.1;

    materialsCount = 0;
    tetrasCount = 0;
    lastPacketCount = 0;
    
    tetras_d = NULL;
    launchedPackets_d = NULL;
    rngState_d = NULL;

    tetraEnergy_d = NULL;
    meshExitEnergy_d = NULL;
    faceEnergy1_d = NULL;
    faceEnergy2_d = NULL;
}

/** Destructor */
CUDAAccel::~CUDAAccel() {
    destroy();
}

/**
 * Initialize the device
 *
 * @param seed the seed for the RNG
 * @param Nstep_max the maximum number of steps a packet can take
 * @param Nhit_max the maximum number of intersections a packet can have in one step
 * @param numFaces the number of faces in the mesh
 * @param wmin the minimum weight before a packet goes to roulette
 * @param prwin the percentage of a roulette win
 * @param materials the list of materials
 * @param tetras the mesh tetras
 * @param copyTetras whether or not to copy the tetras to the GPU (saves time if they are already there)
 * @returns true on success, false on failure
 */
bool CUDAAccel::init(
    const unsigned              seed,
    const unsigned              Nstep_max,
    const unsigned              Nhit_max,
    const unsigned              numFaces,
    const float                 wmin,
    const float                 prwin,
    const vector<Material>      &materials,
    const vector<Tetra>         &tetras,
    const bool                  copyTetras
)
{
    // in case the user didn't change the device we 'change'
    // the device to the current one.
    changeDevice(devId);

    // check if device is cuda capable
    if(!CUDAAccel::isDeviceCapable(devId)) {
        cerr << "Selected CUDA device is not capable of running FullMonteCUDA" << endl;
        return false;
    }

    // initialize scalaras
    this->seed = seed;
    this->NstepMax = Nstep_max;
    this->NhitMax = Nhit_max;
    this->facesCount = numFaces;
    this->wMin = wmin;
    this->prWin = prwin;
    this->materialsCount = materials.size();
    this->tetrasCount = tetras.size();

    // we store the materials in the constant texture cache of the GPU
    // which is an array of a fixed (max) size. Make sure we don't exceed the size
    if(materials.size() > MAX_CUDA_MATERIALS) {
        cerr << "You have " << materials.size() << " defined but the max CUDAAccel can handle is " << MAX_CUDA_MATERIALS << endl;
        return false;
    }

    // check if memory is already allocated and free it if so
    if(tetraEnergy_d != NULL) {
        checkCudaErrors(cudaFree(tetraEnergy_d));
        tetraEnergy_d = NULL;
    }
    if(meshExitEnergy_d != NULL) {
        checkCudaErrors(cudaFree(meshExitEnergy_d));
        meshExitEnergy_d = NULL;
    }
    if(faceEnergy1_d != NULL) {
        checkCudaErrors(cudaFree(faceEnergy1_d));
        faceEnergy1_d = NULL;
    }
    if(faceEnergy2_d != NULL) {
        checkCudaErrors(cudaFree(faceEnergy2_d));
        faceEnergy2_d = NULL;
    }

    // allocate GPU memory for the tetras and copy to the device if tetras need to be copied
    if(tetras_d == NULL || copyTetras) {
        // free tetras if need be
        if(tetras_d != NULL) {
            checkCudaErrors(cudaFree(tetras_d));
            tetras_d = NULL;
        }

        // allocate memory for the tetras
        checkCudaErrors(cudaMalloc(
            &tetras_d,
            tetrasCount * sizeof(Tetra)
        ));

        // copy the tetras to the device
        checkCudaErrors(cudaMemcpy(
            tetras_d,
            &tetras[0],
            tetrasCount * sizeof(Tetra),
            cudaMemcpyHostToDevice
        ));
    }
    

    // allocate space for volume accumulation array if enabled
    if(scoreVolume) {
        // allocate space
        checkCudaErrors(cudaMalloc(
            &tetraEnergy_d,
            tetrasCount * sizeof(double)
        ));

        // initialize all energies to 0
        checkCudaErrors(cudaMemset(
            tetraEnergy_d,
            0.0,
            tetrasCount * sizeof(double)
        ));
    }

    // allocate space for directed surface accumulation arrays if enabled
    if(scoreDirectedSurface && facesCount > 0) {
        // allocate space
        checkCudaErrors(cudaMalloc(
            &faceEnergy1_d,
            facesCount * sizeof(double)
        ));
        // initialize to 0
        checkCudaErrors(cudaMemset(
            faceEnergy1_d,
            0.0,
            facesCount * sizeof(double)
        ));
        
        // allocate space
        checkCudaErrors(cudaMalloc(
            &faceEnergy2_d,
            facesCount * sizeof(double)
        ));
        // initialize to 0
        checkCudaErrors(cudaMemset(
            faceEnergy2_d,
            0.0,
            facesCount * sizeof(double)
        ));
    }

    // allocate space for surface exit events if enabled
    if(scoreSurfaceExit && facesCount > 0) {
        // allocate space
        checkCudaErrors(cudaMalloc(
            &meshExitEnergy_d,
            facesCount * sizeof(double)
        ));
        // initialize to 0
        checkCudaErrors(cudaMemset(
            meshExitEnergy_d,
            0.0,
            facesCount * sizeof(double)
        ));
    }

    // copy materials to GPU constant cache (see device/full_monte_device.cu)
    copyMaterialsToCache(materials);

    // we don't use much shared memory, so prefer L1 cache over the shared memory
    checkCudaErrors(cudaDeviceSetCacheConfig(cudaFuncCachePreferL1));

    return true;
}

/**
 * Launches the simulation (async) by:
 *  1) Creating and initializing CUDA input/output buffers
 *  2) Set the kernel arguments
 *  3) Launching the kernels
 *
 * If you want to wait for the kernel use CUDAAccel::sync()
 *
 * @param launched_packets the packets already launched by an emitter (in host)
 * @return true on success to launch, false on fail
 */
bool CUDAAccel::launch(const vector<LaunchPacket>& launched_packets) {
    printf("CUDAAccel is setting up and launching kernels\n");
    
    // infer the number of packets to launch by the size of the input vector
    const unsigned int num_packets = launched_packets.size(); 

    // that was easy ;)
    if(num_packets <= 0) {
        printf("CUDAAccel WARNING: received a request to launch 0 packets.\n");
        return true;
    }

    // determine how many threads and blocks we will run
    const unsigned int threads = num_packets > prefThreadsPerBlock ? prefThreadsPerBlock : num_packets;
    const unsigned int blocks = (num_packets + threads - 1) / threads;
    
    // make sure we can do this many packets
    if(blocks > maxBlocks) {
        printf("CUDAAccel ERROR: Tried to launch %d blocks but maximum is %d\n", blocks, maxBlocks);
        return false;
    }

    // free old space for launch variables
    if(launchedPackets_d != NULL) {
        checkCudaErrors(cudaFree(launchedPackets_d));
        launchedPackets_d = NULL;
    }
    if(rngState_d != NULL) {
        checkCudaErrors(cudaFree(rngState_d));
        rngState_d = NULL;
    }

    // allocate new space for launch variables
    checkCudaErrors(cudaMalloc(
        &launchedPackets_d,
        num_packets * sizeof(LaunchPacket)
    ));
    checkCudaErrors(cudaMalloc(
        &rngState_d,
        num_packets * sizeof(tinymt32_status_t)
    ));
    
    // copy the launched packets to the device
    checkCudaErrors(cudaMemcpy(
        launchedPackets_d,
        &launched_packets[0],
        num_packets * sizeof(LaunchPacket),
        cudaMemcpyHostToDevice
    ));

    // mark how many packets we are currently simulating in case we call this function again for the same sim
    lastPacketCount = num_packets;

    // check some things before we launch
    if(scoreDirectedSurface && facesCount == 0) {
        printf("CUDAAccel WARNING: scoreDirectedSurface is true but facesCount==0. Turning off DirectedSurface scoring.\n");
        scoreDirectedSurface = false;
    }
    if(!scoreDirectedSurface && !scoreVolume) {
        printf("CUDAAccel WARNING: About to launch but not set to accumulating anything...\n");
    }
    
    // launch the kernel (async)
    full_monte_device_caller(
        seed,
        blocks,
        threads,

        num_packets,
        NstepMax,
        NhitMax,
        wMin,
        prWin,
        launchedPackets_d,
        tetras_d,
        rngState_d,

        scoreVolume,
        scoreDirectedSurface,
        scoreSurfaceExit,

        tetraEnergy_d,
        meshExitEnergy_d,
        faceEnergy1_d,
        faceEnergy2_d
    );

    // check kernel launch error code
    checkCudaErrors(cudaPeekAtLastError());

    return true;
}

/**
 * Waits for the kernel(s) to finish running.
 * Should be called when you want to wait for a CUDAAccl::launch to finish.
 */
void CUDAAccel::sync() {
    // wait for everything to finish
    // NOTE: this simply returns if nothing is in the queue to be run on the GPU.
    checkCudaErrors(cudaDeviceSynchronize());
}

/**
 * cleanup after running the CUDA device. Frees memory.
 */
void CUDAAccel::destroy() {
    // free cuda allocated memory
    if(launchedPackets_d != NULL) {
        checkCudaErrors(cudaFree(launchedPackets_d));
    }

    if(tetras_d != NULL) {
        checkCudaErrors(cudaFree(tetras_d));
    }

    if(tetraEnergy_d != NULL) {
        checkCudaErrors(cudaFree(tetraEnergy_d));
    }

    if(meshExitEnergy_d != NULL) {
        checkCudaErrors(cudaFree(meshExitEnergy_d));
    }

    if(faceEnergy1_d != NULL) {
        checkCudaErrors(cudaFree(faceEnergy1_d));
    }

    if(faceEnergy2_d != NULL) {
        checkCudaErrors(cudaFree(faceEnergy2_d));
    }
    
    if(rngState_d != NULL) {
        checkCudaErrors(cudaFree(rngState_d));
    }
}

/**
 * Copies the energy of the tetras computed by the device into a host array.
 * NOTE: You MUST call CUDAAccel::sync (i.e. cudaDeviceSynchronize()) before this
 * or you will have some problems!
 *
 * @param tetraEnergy the host tetra energies to copy into.
 */
void CUDAAccel::copyTetraEnergy(vector<double>& tetraEnergy) {
    // ensure the sizes are the same
    tetraEnergy.resize(tetrasCount);

    // copy the data
    checkCudaErrors(cudaMemcpy(
        &tetraEnergy[0],
        tetraEnergy_d,
        tetrasCount * sizeof(double),
        cudaMemcpyDeviceToHost
    ));
}

/**
 * Copies the energy of the tetras computed by the device into a host array.
 * The host may use floats after the absorption is done so this is for convenience.
 * See CUDAAccel::copyTetraEnergy(<float>)
 *
 * @param tetraEnergy the host tetra energies to copy into.
 */
void CUDAAccel::copyTetraEnergy(vector<float>& tetraEnergy) {
    // ensure enough space
    tetraEnergy.resize(tetrasCount);
    
    // copy doubles from device
    vector<double> tmp(tetrasCount);
    copyTetraEnergy(tmp);

    // convert to floats
    for(unsigned t = 0; t < tetrasCount; t++) {
        tetraEnergy[t] = (float)tmp[t];
    }
}


/**
 * Copies the energy exiting the surface of the mesh to the host array.
 *
 * @param surfaceExitEnergy the array to fill with surface exit energy
 */
void CUDAAccel::copySurfaceExitEnergy(vector<double>& surfaceExitEnergy) {
    if(facesCount > 0) {
        // ensure enough space
        surfaceExitEnergy.resize(facesCount);

        // copy the data
        checkCudaErrors(cudaMemcpy(
            &surfaceExitEnergy[0],
            meshExitEnergy_d,
            facesCount * sizeof(double),
            cudaMemcpyDeviceToHost
        ));
    }
}

/**
 * Copies the energy exiting the surface of the mesh to the host array.
 *
 * @param surfaceExitEnergy the array to fill with surface exit energy
 */
void CUDAAccel::copySurfaceExitEnergy(vector<float>& surfaceExitEnergy) {
    if(facesCount > 0) {
        // ensure enough space
        surfaceExitEnergy.resize(facesCount);

        // copy doubles to device
        vector<double> tmp(facesCount);
        copySurfaceExitEnergy(tmp);

        // convert to floats
        for(unsigned f = 0; f < facesCount; f++) {
            surfaceExitEnergy[f] = (float)tmp[f];
        }
    }
}


/** 
 * Copies the directed face energy computed by the device into host arrays.
 *
 * @param withNormal the energy passing through the face along the normal.
 * @param antiNormal the energy passing through the face against the normal.
 */
void CUDAAccel::copyDirectedSurfaceEnergy(vector<double>& withNormal, vector<double>& antiNormal) {
    if(facesCount > 0) {
        // ensure sizes match
        withNormal.resize(facesCount);
        antiNormal.resize(facesCount);

        // copy data
        checkCudaErrors(cudaMemcpy(
            &withNormal[0],
            faceEnergy1_d,
            facesCount * sizeof(double),
            cudaMemcpyDeviceToHost
        ));
        checkCudaErrors(cudaMemcpy(
            &antiNormal[0],
            faceEnergy2_d,
            facesCount * sizeof(double),
            cudaMemcpyDeviceToHost
        ));
    }
}

/** 
 * Copies the directed face energy computed by the device into host arrays.
 *
 * @param withNormal the energy passing through the face along the normal.
 * @param antiNormal the energy passing through the face against the normal.
 */
void CUDAAccel::copyDirectedSurfaceEnergy(vector<float>& withNormal, vector<float>& antiNormal) {
    if(facesCount > 0) {
        // ensure sizes match
        withNormal.resize(facesCount);
        antiNormal.resize(facesCount);
        
        // copy doubles from the device
        vector<double> tmp1(facesCount);
        vector<double> tmp2(facesCount);
        copyDirectedSurfaceEnergy(tmp1, tmp2);

        //convert to floats
        for(unsigned f = 0; f < facesCount; f++) {
            withNormal[f] = (float)tmp1[f];
            antiNormal[f] = (float)tmp2[f];
        }
    }
}

/**
 * This function returns the maximum number of packets that can be done in a single run of the kernel.
 * It uses the number of tetras and determines how much memory remains and how many packets can fit in the devices memory.
 * It also checks the packets against the maximum number of threads that can be run in one kernel (from CUDA runtime)
 *
 * @returns the (conservative) maximum number of packets simulated in one 'CUDAAccel::launch' call.
 */
unsigned int CUDAAccel::maxPacketsPossible() {
    // get the device properties for the current GPU
    cudaDeviceProp prop;
    checkCudaErrors(cudaGetDeviceProperties(&prop, devId));

    // determine how many threads we can launch based on how much memory we have
    const size_t faceBytes = (scoreSurfaceExit + scoreDirectedSurface) * facesCount; 
    const size_t tetraBytes = (sizeof(Tetra) + scoreVolume*sizeof(double)) * tetrasCount;
    const size_t bytesPerPacket = sizeof(LaunchPacket) + sizeof(tinymt32_status_t);
    const unsigned int maxPacketsMem = (prop.totalGlobalMem - (tetraBytes + faceBytes)) / bytesPerPacket;
    const unsigned int safeMaxPacketsMem = floor(MEMORY_SAFETY_FACTOR * maxPacketsMem);

    // determine how many threads we can launch based on the max number of threads for the device
    const unsigned int maxPacketsThreads = prefThreadsPerBlock * maxBlocks;

    // use the minimum of the two maximums above
    return min(safeMaxPacketsMem, maxPacketsThreads); 
}

/**
 * Changes the cuda device based on the device ID.
 * List the devices using the listCudaDevices function.
 *
 * @param devid the ID of the CUDA device to use.
 */
void CUDAAccel::changeDevice(int devId) {
    this->devId = devId;
    checkCudaErrors(cudaSetDevice(devId));
    
    // get properties of device
    cudaDeviceProp prop;
    checkCudaErrors(cudaGetDeviceProperties(&prop, devId));
    maxThreadsPerBlock = prop.maxThreadsPerBlock;
    maxBlocks = prop.maxGridSize[0];

    // set perferred number of threads per block.
    // in a simple kernel we would use the max_threads_per_block
    // but, since our kernel is complicated, we must use less threads.
    // TODO: do this dynamically? do it better ...
    prefThreadsPerBlock = maxThreadsPerBlock / 16;
}

/**
 * Try to determine the maximum number of tetras that the CURRENT device can handle
 * based on the size of a Tetra and the amount of memory on the CURRENT device.
 * 
 * @param packetCountHint hint at the number of packets wished to be run - default is 0
 * @returns an ESTIMATE of the max number of tetras that the device can hold.
 */
size_t CUDAAccel::maxTetraEstimate(unsigned int packetCountHint) {
    // get the device properties for the current GPU
    cudaDeviceProp prop;
    checkCudaErrors(cudaGetDeviceProperties(&prop, devId));

    const size_t bytesPerTetra = sizeof(Tetra) + 3*sizeof(double);
    const size_t packetBytes = packetCountHint * (sizeof(LaunchPacket) + sizeof(tinymt32_status_t));
    
    // estimate of the max number of tetras and use safe amount
    const size_t maxTetras = (prop.totalGlobalMem - packetBytes) / bytesPerTetra;
    return floor(MEMORY_SAFETY_FACTOR * (float)(maxTetras));
}


/**
 * Determines if the currently set CUDA device is capable of runnning FullMonte
 *
 * @returns whether the devices is capable of running FullMonte with CUDA acceleration
 */
bool CUDAAccel::isDeviceCapable() {
    return CUDAAccel::isDeviceCapable(this->devId);
}

/**
 * STATIC
 * Given the device ID of a CUDA device, determine if it is capable of running FullMonte
 *
 * @param devId the device ID (printed by 'listCudaDevices') of the CUDA device.
 * @returns whether the device is capable of running FullMonte with CUDA acceleration
 */
bool CUDAAccel::isDeviceCapable(int devId) {
    cudaDeviceProp prop;
    checkCudaErrors(cudaGetDeviceProperties(&prop, devId));

    bool computeCapable = ((prop.major > MIN_COMPUTE_CAPABILITY_MAJOR) || (prop.major == MIN_COMPUTE_CAPABILITY_MAJOR && prop.minor >= MIN_COMPUTE_CAPABILITY_MINOR));
    bool inComputeMode = (prop.computeMode != cudaComputeModeProhibited);

    return computeCapable && inComputeMode;
}


/**
 * STATIC
 * Uses CUDA runtime SDK to list all of the CUDA capable devices availible.
 */
void CUDAAccel::listDevices() {
    printf("\n");
    int driverVersion = 0, runtimeVersion = 0, devCount = 0;
    checkCudaErrors(cudaGetDeviceCount(&devCount));

    for (int dev = 0; dev < devCount; dev++) {
        checkCudaErrors(cudaSetDevice(dev));

        cudaDeviceProp prop;
        checkCudaErrors(cudaGetDeviceProperties(&prop, dev));
        checkCudaErrors(cudaDriverGetVersion(&driverVersion));
        checkCudaErrors(cudaRuntimeGetVersion(&runtimeVersion));

        printf("====================\n");
        printf("Device ID: %d\n", dev);
        printf("Device Name: %s\n", prop.name);
        printf("Compute Capability: %d.%d\n", prop.major, prop.minor);
        printf("CUDA Driver Version / Runtime Version: %d.%d / %d.%d\n", driverVersion / 1000, (driverVersion % 100) / 10, runtimeVersion / 1000, (runtimeVersion % 100) / 10);
        printf("Device is capable of running FullMonteCUDA: %s\n", isDeviceCapable(dev) ? "Yes" : "No");
        printf("====================\n\n");
    }
}

/**
 * @returns the current GPU device ID (devId).
 */
int CUDAAccel::currentDeviceID() {
    return devId;
}

