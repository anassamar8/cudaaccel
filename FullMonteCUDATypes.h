#ifndef _TYPES_H_
#define _TYPES_H_

/**
 * This file defines all the custom types used by CUDAAccel
 */

#include "vector_types.h"  // from NVIDIA

// defined FMALIGN for different compilers
// TODO: CLANG?
#if defined(__CUDACC__) // NVCC
    #define FMALIGN(n) __align__(n)
#elif defined(__GNUC__) // GCC
    #define FMALIGN(n) __attribute__((aligned(n)))
#elif defined(_MSC_VER) // MSVC
    #define FMALIGN(n) __declspec(align(n))
#else
    #error "Please provide a definition for FMALIGN macro for your host compiler!"
#endif

/**
 * The CUDA namespace.
 * When incorporated into FullMonteSW this distringuishes between, for example,
 * the 'Tetra' used natively in FullMonteSW and the 'Tetra' used for the CUDA kernels.
 */
namespace CUDA {

/**
 * PacketDirection
 * The direction of a packet.
 * The 'd' vector is the direction while 'a' and 'b' are always orthogonal
 * to 'd' and each other (orthonormal).
 */
typedef struct {
    // Direction vector
    float3              d;

    // First unit auxilary vector orthonormal to the direction of travel d and b
    float3              a;
    
    // Second unit auxilary vector orthonormal to the direction of travel d and a
    float3              b;
} PacketDirection;


/**
 * Packet
 * A packet being simulated.
 */
typedef struct {
    // The current direction of the packet
    PacketDirection     dir;

    // Position of the packet [x, y, z]
    float3              p;
    
    // Dimensionless step length remaining
    // [physical distance, Mean Free Paths (MFPs) remaining to go, time]
    float3              s;
    
    // Current packet weight
    float               w;
} Packet;

/**
 * LaunchPacket
 * A packet specifically at the moment of launch
 * It has no step length (not drawn yet) or weight (implicitly 0)
 */
typedef struct {
    // The current direction of the packet
    PacketDirection     dir;

    // Position of the packet [x, y, z]
    float3              p;

    // The tetra ID
    unsigned int        tetraID;    
} LaunchPacket;

/**
 * StepResult
 * Represents the result of a step in the simulation.
 */
typedef struct {
    // Position (x, y, z) of the next position of the packet
    float3          Pe;

    // Distance to the nearest face of the tetra where the packet is in
    // or the random step length for the packet
    float           distance;

    // Face ID of the tetras face which has the smallest distance to the
    // packets current position.
    int             IDfe;

    // Tetra ID of the tetra which is adjacent to the face which is nearest
    // to the packets current position (IDfe)
    unsigned int    IDte;

    // The index of the nearest face to the packet
    int             idx;

    // Boolean value: True if the packet intersects with the face of the current
    // tetra within the random step length. I.e. the packet leaves the current tetra
    bool            hit;
} StepResult;

/**
 * Material
 * Represents a material in the mesh
 * TODO: Lots of repeated information here, could trim some
 */
typedef struct {
    // scattering coefficient
    float               mu_s;  // TODO: not used (just computes mu_t)

    // absorbing coefficient
    float               mu_a;  // TODO: not used (just computes mu_t)

    // g in different forms (all are used)
    float               g;
    float               gg;
    float               recip_2g;
    float               one_minus_gg;
    float               one_plus_gg;

    // refractive index
    float               n;

    // propagation vector along unit physical step: time elapsed increases,
    // dimensionless step decreases by mu_t, physical step decreases by 1
    float3              m_prop;

    // initial propogation vector
    // (1/mu_t physical step remaining, 1 dimensionless step remaining, 0 time)
    float3              m_init;
    
    // mu_a + mu_s
    float               mu_t;
    float               recip_mu_t;

    // fraction absorbed at each interaction: mu_a / mu_t
    float               absfrac;

    // does the material scatter: mu_s != 0
    bool                scatters;  // TODO: not used

    // it the material istropic (g ~= 0.0)
    bool                isotropic;
} Material;


/**
 * Tetra
 * Tetrahedron representation
 * Normals (nx, ny, nz) are oriented so that points inside 
 * the tetra have a positive altitude (h=dot(p,n)-C)
 */
typedef struct {
    // the x, y, z components of the 4 face normals
    float4          nx;
    float4          ny;
    float4          nz;

    // Constants of all 4 tetra faces. Used to check if a point is inside the Tetra
    float4          C;

    // The four directed face IDs of the current tetra extracted from the array of faces
    uint4           IDfds;

    // Adjacent tetras ID for each face of the current tetra
    uint4           adjTetras;

    // Material ID for this tetra
    unsigned int    matID;

    // Flags for each face (4Byte = 32Bit -> 8Bit each)
    // For face i=0..3, content of (faceFlags >> (i<<3)) is:
    // Bit  Meaning
    // ===  =======
    // 7
    // 6
    // 5
    // 4
    // 3
    // 2
    // 1
    // 0    whether to score fluence through the surface (DirectedSurfaceScorer)
    //
    unsigned int    faceFlags;
} FMALIGN(128) Tetra;

}  // END: CUDA namespace

#endif

