# Introduction
CUDAAccel is a hardware accelerator for NVIDIA GPUs written in C++/CUDA. 
FullMonte is a flexible and accurate simulator for light propagation in complex media, such as tissue.

# Dependencies
- CUDA SDK: Currently using version 10.0 but others could (should) work. Instructions for downloading the SDK are provided by NVIDIA [here](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html).
- CMake: Currently using version 3.12.0

# Directory Structure
This directory (the root of the project) contains the following directories and files:
- **CUDAAccel{.cpp/.h}**: The API for interacting with the accelertor. Exposes functions to initialize, call, read the results and cleanup the accelerator.
- **helper_cuda.h**: Helper functions for CUDA runtime stuff provided by NVIDIA.
- **helper_string.h**: Helper file for helper_cuda.h
- **FullMonteCUDAConstants.h**: FullMonte constants particular to the CUDA acclerator. 
- **FullMonteCUDATypes.h**: The datatypes (Tetra, Material, Packet, etc) used inside the CUDA kernels. Needed by host and device. It is up to the host (FullMonteSW) to make sure these datatypes are set up correctly.
- **CMakeLists.txt**: The CMake file for the library (used by FullMonteSW)
- **README.md**: This file.
- **kernels/**: All of the CUDA kernels.

# kernels
Inside the 'kernels' subdirectory there are the following files and directories:
- **full_monte_device.cu**: The main kernel file for the accelerator which is compiled by NVCC. The main pipeline is in here.
- **full_monte_device.cuh**: The header file for the main file. This is included in the API files to expose the kernels to call.
- **full_monte_internels.cu**: Functions internal to the kernels such as *scatter*, *reflect_refract* and *getIntersection*. This file is mainly to keep the main file clean and modular.
- **helper_math.h**: Helper math functions (e.g. dot, cross and math operators) provided by NVIDIA for CUDA vector datatypes like float3 and float4.
- **tinymt**: The CUDA Mersenne Twister implementation from [here](http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/TINYMT/index.html)

# Compute capability
- Currently we target compute capability 6.1. We require double precision atomics so that, in theory, limits us to compute capability 3.1 and higher. If different/additional compute capabilities want to be targeted, we will have to add the architecture by using the `nvcc` flags.


